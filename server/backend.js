const gameController = require('./game-controller.js')
const calculator = require('./calculator.js')

const getGameIdFromSocket = (socket) => {
	// Get the URL from the socket object so we can parse the game ID from it
	const { client: { request: { headers: { referer: connectionUrl } } } } = socket // ES6 destructuring style
	return connectionUrl.substr(connectionUrl.lastIndexOf('/') + 1).toLowerCase()
}

const getUrlBaseFromSocket = (socket) => {
	const { client: { request: { headers: { referer: connectionUrl } } } } = socket // ES6 destructuring style
	return connectionUrl.substr(0, connectionUrl.lastIndexOf('/') + 1)
}

exports.onboardConnection = (io, socket) => {
	const gameId = getGameIdFromSocket(socket)

	if (gameId) {
		// Player events
		socket.on('joinGame', (data) => {
			if (gameController.hasGameStarted(gameId)) {
				console.log(socket.id + ' tried to join game #' + gameId + ' but the game had already started')
			} else {
				socket.join(gameId)
				gameController.addNewPlayer(data, socket.id)
				io.in(gameId).emit('updatePlayers', gameController.getPlayers(gameId))
				console.log(socket.id + ' successfully joined game #' + gameId)
			}
		})
		socket.on('startGame', (data) => {
			gameController.startGame(data)
			io.in(gameId).emit('gameStarted')
			console.log(socket.id + ' successfully started game #' + gameId)
		})
		socket.on('pointGun', (data) => {
			const { targetId, usedBullet } = data
			calculator.updateAimingInfo(
				gameController.getAimingInfo(gameId),
				gameController.getPlayerBySocket(gameId, socket).playerId,
				targetId,
				usedBullet
			)
			io.in(gameId).emit('updateRoundInfo', gameController.getGame(gameId).round)
		})
		socket.on('backOff', (data) => {
			calculator.updateBackOffInfo(
				gameController.getBackOffInfo(gameId),
				gameController.getPlayerBySocket(gameId, socket).playerId,
				data
			)
			io.in(gameId).emit('updateRoundInfo', gameController.getGame(gameId).round)
		})
		socket.on('startNewGame', (data) => {
			console.log(data)
		})
	} else {
		// Big Screen logic
		const newGameId = gameController.getNewGameId()
		socket.join(newGameId)
		gameController.initNewGame(newGameId)
		console.log(socket.id + ' successfully created a new game: #' + newGameId)
		socket.emit('onboarded', {
			urlBase: getUrlBaseFromSocket(socket),
			gameId: newGameId
		})
		socket.on('startRound', () => {
			socket.emit('roundStarted')
		})
		socket.on('startPointingGun', () => {
			io.in(newGameId).emit('playerStartPointingGun')
		})
		socket.on('stopPointingGun', () => {
			io.in(newGameId).emit('playerStopPointingGun')
		})
		socket.on('getRoundInfo', () => {
			socket.emit('updateRoundInfo', gameController.getGame(newGameId).round)
		})
		socket.on('startBackOff', () => {
			io.in(newGameId).emit('playerStartBackOff')
		})
		socket.on('stopBackOff', () => {
			io.in(newGameId).emit('playerStopBackOff')
		})
		socket.on('calculateRoundEnd', () => {
			calculator.calculateDamageInfo(
				gameController.getGame(newGameId),
				gameController.getAimingInfo(newGameId),
				gameController.getBackOffInfo(newGameId),
				gameController.getDamageInfo(newGameId)
			)
			calculator.updateLives(
				gameController.getGame(newGameId),
				gameController.getDamageInfo(newGameId)
			)
			calculator.updatePlayerAmmo(
				gameController.getGame(newGameId),
				gameController.getAimingInfo(newGameId),
				gameController.getBackOffInfo(newGameId)
			)
			calculator.updateWinners(gameController.getGame(newGameId))
			io.in(newGameId).emit('updatePlayers', gameController.getPlayers(newGameId))
			socket.emit('updateRoundInfo', gameController.getGame(newGameId).round)
		})
		socket.on('phaseEndRound', () => {
			io.in(newGameId).emit('endRound')
			calculator.clearRoundData(gameController.getGame(newGameId))
			socket.emit('roundStarted')
		})
	}
}
