const bases = require('bases')
const Loki = require('lokijs')
const db = new Loki('loki.json')
const gamesDb = db.addCollection('games')

const decimalGameIds = []

const maxPlayers = 8

const alphabet = 'abcdefghijklmnpqrstuvwxyz' // Missing 'o' for readability
exports.alphabet = alphabet

function Player(name, playerId, socketId) {
	this.name = name
	this.socketId = socketId
	this.playerId = playerId
	this.loot = []
	this.lives = 3
	this.ammo = {
		dud: 5,
		bang: 3,
	}
}

const games = {
	get(gameId) {
		return gamesDb.findOne({ gameId })
	},
	initialize(newGameId) {
		const game = {
			gameId: newGameId,
			started: false,
			players: [],
			loot: [],
			round: {
				aimingInfo: {},
				backOffInfo: {},
				damageInfo: {},
				loot: [],
				winners: []
			}
		}
		gamesDb.insert(game)
	},
	start(gameId) {
		const game = gamesDb.findOne({ gameId })
		game.started = true
		gamesDb.update(game)
	},
	addNewPlayer(gameId, player) {
		const game = gamesDb.findOne({ gameId })
		game.players.push(player)
		gamesDb.update(game)
	},
	hasStarted(gameId) {
		const game = gamesDb.findOne({ gameId })
		return game.started
	},
	getPlayers(gameId) {
		const game = gamesDb.findOne({ gameId })
		return game.players
	},
	getAimingInfo(gameId) {
		const game = gamesDb.findOne({ gameId })
		return game.round.aimingInfo
	},
	getBackOffInfo(gameId) {
		const game = gamesDb.findOne({ gameId })
		return game.round.backOffInfo
	},
	getDamageInfo(gameId) {
		const game = gamesDb.findOne({ gameId })
		return game.round.damageInfo
	},
	getWinners(gameId) {
		const game = gamesDb.findOne({ gameId })
		return game.round.winners
	}
}

const isGameIdInUse = (gameId) => {
	return decimalGameIds.includes(bases.fromAlphabet(gameId, alphabet))
}

const generateGameID = () => {
	function generateDecimalGameId() {
		for (let i = 0; i < decimalGameIds.length + 1; i++) {
			if (decimalGameIds.indexOf(i) === -1) {
				decimalGameIds.push(i)
				return i
			}
		}
	}
	const decimalGameId = generateDecimalGameId()
	return bases.toAlphabet(decimalGameId, alphabet)
}

const getPlayerBySocket = (gameId, socket) => {
	return games.getPlayers(gameId).find((player) => {
		return player.socketId === socket.id
	})
}

exports.getNewGameId = () => {
	return generateGameID()
}

exports.initNewGame = (newGameId) => {
	games.initialize(newGameId)
}

exports.getPlayers = (gameId) => {
	return games.getPlayers(gameId)
}

exports.startGame = (data) => {
	const { gameId } = data
	games.start(gameId)
}

exports.addNewPlayer = (data, socketId) => {
	const { playerName, gameId } = data
	const newPlayer = new Player(playerName, games.getPlayers(gameId).length, socketId)
	games.addNewPlayer(gameId, newPlayer)
}

exports.hasGameStarted = (gameId) => {
	return games.hasStarted(gameId)
}

exports.isGameFull = (gameId) => {
	return games.getPlayers(gameId).length >= maxPlayers
}

exports.getPlayerBySocket = getPlayerBySocket

exports.isGameIdInUse = isGameIdInUse

exports.getGame = (gameId) => {
	return games.get(gameId)
}

exports.getAimingInfo = (gameId) => {
	return games.getAimingInfo(gameId)
}

exports.getBackOffInfo = (gameId) => {
	return games.getBackOffInfo(gameId)
}

exports.getDamageInfo = (gameId) => {
	return games.getDamageInfo(gameId)
}

exports.getWinners = (gameId) => {
	return games.getWinners(gameId)
}
