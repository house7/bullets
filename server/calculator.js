/* eslint-disable capitalized-comments */
/**
 * Created by azatris on 08-Apr-17.
 * Includes static methods for dealing with game logic
 * Stateless
 */

exports.updateAimingInfo = (aimingInfo, aimingPlayerId, targetPlayerId, usedBullet) => {
	const pointedGun = {
		aimingPlayerId,
		targetPlayerId,
		usedBullet
	}
	aimingInfo[aimingPlayerId] = pointedGun
}

exports.updateBackOffInfo = (backOffInfo, backedOffPlayerId, hasPlayerBackedOff) => {
	backOffInfo[backedOffPlayerId] = hasPlayerBackedOff
}

/**
 * aimingInfo is an object of playerIds containing aimingPlayerId, targetPlayerId and usedBullet:
 * {
 *    0: {
 *       aimingPlayerId: 0,
 *       targetPlayerId: 1,
 *       usedBullet: 'dud'
 *    },
 *    1: {
 *       aimingPlayerId: 1,
 *       targetPlayerId: 0,
 *       usedBullet: 'bang'
 *    }
 * }
 *
 * backOffInfo is an object of playerId-and-backOffInfo pairs - Whether they backed off or not
 * {
 *    0: false,
 *    1: true,
 *    2: true
 * }
 */
exports.calculateDamageInfo = (game, aimingInfo, backOffInfo, damageInfo) => {
	game.players.forEach((player) => {
		const targetPlayerID = aimingInfo[player.playerId].targetPlayerId
		if (aimingInfo[player.playerId].usedBullet === 'bang' && !backOffInfo[targetPlayerID] && !backOffInfo[player.playerId]) {
			damageInfo[targetPlayerID] = damageInfo[targetPlayerID] + 1 || 0
		}
	})
	game.players.forEach((player) => {
		if (damageInfo[player.playerId] === undefined) {
			damageInfo[player.playerId] = 0
		}
	})
}

/**
 * damageInfo is an object of playerID-and-receivedDamage pairs:
 * {
 *    0: 0,
 *    1: 3,
 *    2: 0,
 *    3: 1
 * }
 */
exports.updateLives = (game, damageInfo) => {
	game.players.forEach((player) => {
		player.lives -= damageInfo[player.playerId]
	})
}

/**
 * winners are an array of player IDs who did not get shot, did not back off and are still alive
 * [0, 2]
 */

exports.updateWinners = (game) => {
	game.players.forEach((player) => {
		if (player.lives > 0 && !game.round.backOffInfo[player.playerId] && game.round.damageInfo[player.playerId] === 0) {
			game.round.winners.push(player.playerId)
		}
	})
}

exports.updatePlayerAmmo = (game, aimingInfo, backOffInfo) => {
	game.players.forEach((player) => {
		if (!backOffInfo[player.playerId]) {
			if (aimingInfo[player.playerId].usedBullet === 'dud') {
				player.ammo.dud--
			} else if (aimingInfo[player.playerId].usedBullet === 'bang') {
				player.ammo.bang--
			}
		}
	})
}

exports.clearRoundData = (game) => {
	const cleanRound = {
		aimingInfo: {},
		backOffInfo: {},
		damageInfo: {},
		loot: [],
		winners: []
	}
	game.round = cleanRound
}
