import Vue from 'vue'
import App from './components/Gameboard.vue'
import VueWebsocket from 'vue-websocket'

Vue.use(VueWebsocket)
new Vue({ // eslint-disable-line no-new
	el: '#app',
	render: (h) => h(App)
})
