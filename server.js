// Packages
const http = require('http')
const express = require('express')
const socketIo = require('socket.io')
const pug = require('pug')

// Init
const backend = require('./server/backend.js')
const gameController = require('./server/game-controller.js')
const app = express()
const server = http.createServer(app)
const port = process.env.PORT || 8090
const io = socketIo(server)

// Static content
app.use('/static', express.static('static'))

// Views
const renderError = pug.compileFile('views/error.pug')
const renderIndex = pug.compileFile('views/index.pug')
const renderPlayer = pug.compileFile('views/player.pug')

// Routes
app.get('/', (req, res) => res.send(renderIndex()))
app.get('/404', (req, res) => {
	res.send(renderError({
		title: '404',
		message: 'Oops! The page you were looking for does not exist!'
	}))
})
app.get('/:gameId([' + gameController.alphabet + ']+)', (req, res) => {
	const { gameId } = req.params
	if (gameController.isGameIdInUse(gameId)) {
		if (gameController.hasGameStarted(gameId)) {
			res.send(renderError({
				title: 'Game in progress',
				message: 'Oops! This game is already in progress!'
			}))
		} else if (gameController.isGameFull(gameId)) {
			res.send(renderError({
				title: 'Game is full',
				message: 'Oops! This game appears to be full!'
			}))
		} else {
			res.send(renderPlayer())
		}
	} else {
		res.status(404).send(renderError({
			title: 'Game not found',
			message: 'We couldn’t find a game with this ID: ' + gameId
		}))
	}
})

// Websocket
io.on('connection', (socket) => {
	console.log(socket.id + ' websocket connected')
	backend.onboardConnection(io, socket)
})

// Start
server.listen(port, () => {
	console.log(`Server listening on port ${port}`)
})
