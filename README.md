# Bullets
Couch multiplayer game of greed, cartoon violence and pushing your luck.

## Motivation
It's a learning experience disguised as a party game. We wanted to make a Node.js project that uses Websockets, but the game design and UX aspects are also fascinating.

## Running
### Dev
```
npm install -g webpack
npm install
npm run dev
```
Or run two separate command lines: `npm run dev:server` and `npm run dev:client`

### Production
```
npm install --production
npm run production
```

## Tests
Right now we have only linting
```
npm run test
```

## Contributors
* Silver Taza
* Joonas Ervald
* [Hardi Kõvamees](http://luminarious.net)

## License
MIT